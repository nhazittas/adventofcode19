from collections import Counter
import sys


for line in sys.stdin:
    inn = line.strip()

start = 0

rows = 6
cols = 25
size = rows*cols
numLayers = len(inn)//size
layers =[]

index = 0
for layer in range(numLayers):
    beg = size*layer
    end = size*(layer+1)
    layers.append(inn[beg:end])

pic= [2]*size

#print(pic)

for layer in layers:
    for index, cell in enumerate(layer):
        #print(pic[index], cell)
        #print("'{}'".format(pic[index]), int(pic[index]) == 2)
        if int(pic[index]) == 2:
            #print("set", index, cell)
            pic[index] = cell

print(pic)

for i in range(rows):
    r = pic[cols*i:cols*(i+1)]
    r = [ " " if int(cell) == 0 else cell   for cell in r]
    print("".join(r))



#ccs = [ Counter(layer) for layer in layers]
#
#fewestZeros = size
#fewestC = -1
#
#for c in ccs:
#    if c['0'] < fewestZeros:
#        fewestZeros = c['0']
#        fewestC = c



#
#print(fewestC['1'], fewestC['2'])
#print(fewestC['1'] * fewestC['2'])
#def countNum(dArray, numToCount):
#    n = sum( x == numToCount for row in dArray for x in row )
#    print(n)
#    return n
#print(layers)
#theLayer = min(layers, key=lambda l: countNum(l, "0"))
#
#print(theLayer)
#
#ones = countNum(theLayer, "1")
#twos = countNum(theLayer, "2")
#
#print(ones, twos, ones*twos)

