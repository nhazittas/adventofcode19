import sys


for line in sys.stdin:
    inn = line.strip()

start = 0

rows = 25
cols = 6
size = rows*cols
numLayers = len(inn)//size
print(numLayers)
layers =[[[0]*6]*25]*numLayers

index = 0
for layer in range(numLayers):
    for row in range(rows):
        for col in range(cols):
            layers[layer][row][col] = inn[index]
            index+=1

def countNum(dArray, numToCount):
    return sum( x == numToCount for row in dArray for x in row )

theLayer = min(layers, lambda l: countNum(l, "0"))

ones = countNum(theLayer, "1")
twos = countNum(theLayer, "2")

print(ones, twos, ones*twos)

