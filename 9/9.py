import sys
from collections import defaultdict

inputs = []
outputs = []

#debug = False
debug = True

printOut = True

rb = 0


def parseParms(program, pc,  numparms):
    global rb

    retVal = []
    options = program[pc]//100
    for i in range(numparms):
        imm = (options % 10) == 1
        rm = (options % 10) == 2
        options//=10

        val = program[(pc +1) + i]
        val = val if imm else ( program[rb+val] if rm else program[val] ) 

        retVal.append(val)
    if debug:
        print("{} {} {} | {} {}".format(program[pc], program[pc+1], program[pc+2], numparms,retVal))
    return retVal


def add(program, pc):
    if debug:
        print("ADD", program[pc])

    a,b = parseParms(program, pc, 2)
    c = program[pc+3]

    program[c] = a + b
    if debug:
        print("\t [",c,"]","=", a+b,"=",a,"+",b)

    return pc + 4

def mult(program, pc):
    a,b = parseParms(program, pc, 2)
    c = program[pc+3]

    if debug:
        print("mult @{}= {}x{}".format(c, a, b) )
    program[c] = a * b

    return pc + 4

def read(program, pc):
    if debug:
        print(inputs)
    ad = program[pc+1]
    i = inputs.pop()
    if debug:
        print("IN[",pc,"]",ad, "=", i)

    program[ad] = i

    return pc + 2

def write(program, pc):
    o = parseParms(program, pc, 1)

    outputs.append(o)
    if printOut:
        print("OUT[", pc,"]",program[pc+1],"=", o)

    return pc + 2

def jit(program, pc):
    a,b = parseParms(program, pc, 2)
    if debug:
        print("jit ?{} => {}".format(a, b) )
    if a != 0:
        pc = b
    else:
        pc+=3

    return pc

def jif(program, pc):
    a,b = parseParms(program, pc, 2)
    if debug:
        print("jif ?!{} => {}".format(a, b) )
    if a == 0:
        pc = b
    else:
        pc+=3

    return pc

def lt(program, pc):
    a,b = parseParms(program, pc, 2)
    c = program[pc+3]

    if debug:
        print("lt {} ?< {}".format(a, b) )
    program[c] = 1 if a < b else 0

    return pc + 4

def eq(program, pc):
    a,b = parseParms(program, pc, 2)
    c = program[pc+3]

    if debug:
        print("eq {} ?= {}".format(a, b) )
    program[c] = 1 if a == b else 0

    return pc + 4

def rbo(program, pc):
    global rb
    a = parseParms(program, pc, 1)

    rb += a[0]
    if debug:
        print("rb+={} = {}".format(a, rb) )

    return pc + 2

def halt(program, pc):
    if debug:
        print("HALT")
        print(program)
    # exit(0)
    return -1

def error(program, pc):
    print("You dun f up aaron")
    exit(1)

# 1 = add
# 2 = mult
# 3 = input
# 4 = output
# 5 = jump if true
# 6 = jump if false
# 7 = less than
# 8 = equals
# 8 = set relatave offset base
# 99  = halt 
funMap = defaultdict(lambda : error, {1:add, 2:mult, 3:read, 4:write, 5:jit, 6:jif, 7:lt, 8:eq, 9:rbo, 99:halt})


def run(program):
    global rb
    pc = 0
    rb = 0
    while pc != -1:
        todo = program[pc] % 100
        #print(pc, todo)
        pc = funMap[todo](program, pc)
    return outputs[-1]


def runWith(phaseSetting, inpt, program):
    inputs.clear()
    inputs.append(inpt)
    inputs.append(phaseSetting)
    #inputs = [inpt, phaseSetting]
    outputs.clear()

    return run(list(program))

for line in sys.stdin:
     program = [int(l) for l in line.split(",")]
program = defaultdict(int, [(index, val) for index, val in enumerate(program)])

inputs.append(1)

print(len(program))
run(program)

#print(program)
