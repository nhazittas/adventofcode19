import sys

# red wire
# blue wire

# {{U,D,L,R}[0-9]+}*

def run(program):
    s = set()
    m = dict()
    t = 0
    x = 0
    y = 0

    dirs = dict()
    
    dirs["U"] = (0,1)
    dirs["D"] = (0,-1)
    dirs["L"] = (-1,0)
    dirs["R"] = (1,0)


    program = program.split(",")
    for ins in program:
        d = ins[0]
        n = int(ins[1:])

        dx, dy = dirs[d]
        # print(d, dx, dy, "|", n)
        for i in range(1,n+1):
            t+=1
            p = (x+dx*i,y+dy*i)
            s.add(p)
            m[p] = t
            # print((x+dx*i,y+dy*i))

        x = x + dx*n
        y = y + dy*n

    return s,m



runs = []
for line in sys.stdin:
    runs.append(run(line))

r1, m1 = runs[0]
r2, m2 = runs[1]

both = r1.intersection(r2)
print(both)
lam = lambda l:m1[l] + m2[l]
m = min(both, key=lam)

print(m, lam(m))
