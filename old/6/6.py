import sys
from collections import defaultdict


aOrbitsB = {}
aHasOrbits = defaultdict(list)



for line in sys.stdin:
    center,orbiting = line.strip().split(")")

    aOrbitsB[orbiting] = center
    aHasOrbits[center].append(orbiting)

def countOrbits(center = "COM", numParentOrbits=0):
    print(center, numParentOrbits)
    return numParentOrbits + sum([countOrbits(child, numParentOrbits+1) for child in aHasOrbits[center]])

#print(aOrbitsB)
#print(aHasOrbits)

#print(countOrbits())




def findSan(youOrbit, distanceSoFar=0, seen=set()):
    print(youOrbit, distanceSoFar, seen)
    if youOrbit == "SAN":
        return distanceSoFar # maybe plus 1
    copy = set(seen)
    copy.add(youOrbit)

    options = aHasOrbits[youOrbit]
    if youOrbit in aOrbitsB.keys():
        options.append(aOrbitsB[youOrbit])

    toVisit = set(options) - copy

    if len(toVisit) == 0:
        return -1

    winners = list(filter(lambda l: l!=-1 , [findSan(op, distanceSoFar+1, copy) for op in toVisit]))
    if len(winners) == 0:
        return -1
    else:
        return min(winners)

print(findSan(aOrbitsB["YOU"])-1)
   

    

